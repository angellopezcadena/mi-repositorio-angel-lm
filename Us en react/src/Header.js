import React,{Component} from 'react';
import './bootstrap-reboot.min.css';
import './index.css';

import logo from './logo.png';


class Header extends Component
{
	render()
	{
		return(
				<div class="cabe">
				<nav class="navbar navbar-expand-lg navbar-light">
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav mr-auto">
				      <li class="nav-item">
				        <a class="nav-link" href="#">Inicio</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">Articulos</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">Contenido</a>
				      </li>
				      <li class="nav-item active">
				        <a class="nav-link" href="#">Nosotros <span class="sr-only">(current)</span></a>
				      </li>
				    </ul>
				    <form class="form-inline my-2 my-lg-0">
				      <img class="logo" src={logo}></img>
				    </form>
				  </div>
				</nav>
				</div>
			)
	}
}

export default Header;