import React,{Component} from 'react';
import './index.css';
import './bootstrap-reboot.min.css';


var stylotarjeta =
	{
			maxWidth: '18rem',
			backgroundColor: '#1B120F',
	};



class Info extends Component
{

	render()
	{
		const titulo = this.props.titulo;
		return(
				
				<div class="card" style={{stylotarjeta}}>
				  <div class="card-header">{titulo}</div>
				  <div class="card-body">
				    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet consectetur justo. Cras maximus nec tellus ac cursus. Sed porta ligula et tempor pellentesque. Duis non ipsum et sem lacinia maximus vel pharetra leo. Vestibulum id dapibus erat. Morbi mi urna, lacinia id turpis sit amet, egestas pretium arcu. </p>
				  </div>
				</div>
				
			
			)
	}
}

export default Info;